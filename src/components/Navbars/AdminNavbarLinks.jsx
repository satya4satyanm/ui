import React, { Component } from "react";
import { Link } from "react-router-dom";

class AdminNavbarLinks extends Component {
  render() {
    return (
      <div className="right-nav">
            <Link to="/login" className="nav-link">Login/Register</Link>
      </div>
    );
  }
}

export default AdminNavbarLinks;
