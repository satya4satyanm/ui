import Register from "views/Register.jsx";
import Login from "views/Login.jsx";
import Dashboard from "views/Dashboard.jsx";
import UserProfile from "views/UserProfile.jsx";
// import TableList from "views/TableList.jsx";
import CreateAdUnit from "views/CreateAdUnit";
// import Maps from "views/Maps.jsx";
import Notifications from "views/Notifications.jsx";
import Team from "views/Team.jsx";

const dashboardRoutes = [
  {
    path: "/register",
    name: "Register",
    icon: "pe-7s-user",
    component: Register,
    layout: "/admin"
  },{
    path: "/login",
    name: "Login",
    icon: "pe-7s-user",
    component: Login,
    layout: "/admin"
  },{
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "User Profile",
    icon: "pe-7s-user",
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/createAdUnits",
    name: "Create AdUnits",
    icon: "pe-7s-note2",
    component: CreateAdUnit,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "pe-7s-bell",
    component: Notifications,
    layout: "/admin"
  },
  {
    path: "/team",
    name: "Team",
    icon: "pe-7s-chat",
    component: Team,
    layout: "/admin"
  }
];

export default dashboardRoutes;
