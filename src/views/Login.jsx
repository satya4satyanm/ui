import { Grid, Row, Col } from "react-bootstrap";
import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Card from "components/Card/Card.jsx";
import { userService } from '../_services';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
        username: '',
        password: '',
        submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
}

handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
}

handleSubmit(e) {
    e.preventDefault();
    const { history } = this.props;

    this.setState({ submitted: true });
    const { username, password } = this.state;
    if (username && password) {
      let user = userService.login(username, password);
      user.then(
        function() {
            history.push('/admin/dashboard');
        }
      )
      
    }
}

  render() {
        const { username, password, submitted } = this.state;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title=""
                category="Start earning by posting ads from 1000s of advertisers."
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div className="content">
                    <Row>
            <Col md={6}> 
                    <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                        {submitted && !username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Login</button>
                        &nbsp;&nbsp;<Link to="/admin/register" className="btn btn-primary">Register</Link>
                    </div>
                </form>
                </Col></Row>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Login;
